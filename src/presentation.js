/* eslint-disable */
// Import React
import React from 'react'
import CodeSlide from 'spectacle-code-slide'
// Import Spectacle Core tags
import {
  Deck,
  Heading,
  Slide,
  Text,
  Table,
  TableRow,
  TableHeader,
  TableHeaderItem,
  TableBody,
  TableItem,
  Image,
  Appear,
  List,
  ListItem,
  Link,
} from 'spectacle'
// Import theme
import createTheme from 'spectacle/lib/themes/default'

import {Image as SolidImage, Link as SolidLink, List as SolidList, Name, Value} from '@solid/react'

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: 'black',
    secondary: '#03A9FC',
    tertiary: 'white',
    quaternary: 'hotpink',
    error: '#a20200'
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

const tableSize = 18;
const URL = ({href, small, ...props}) => <Link textColor="tertiary" textSize={small ? 20 : undefined} {...props} href={href}>{href}</Link>

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            JSON-LD
          </Heading>
          <Text textColor="tertiary">JSON for Linking Data</Text>
        </Slide>

        <Slide>
          <div style={{display: 'flex'}}>
            <SolidImage style={{borderRadius: 50}} src="[https://angelo.veltens.org/profile/card#me].image"/>

            <div>
              <Heading textColor="secondary" size={4}><Value src="[https://angelo.veltens.org/profile/card#me].name"/></Heading>
              <Heading size={6} textColor="tertiary"><Value
                src="[https://angelo.veltens.org/profile/card#me].vcard_role"/></Heading>
              <URL href="https://angelo.veltens.org/profile/card#me" small style={{marginTop: '1rem'}} />
            </div>
          </div>
        </Slide>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/json.example")}
          ranges={[
            { loc: [0, 10], title: "JSON data" },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/client.example")}
          ranges={[
            { loc: [0, 5], title: "Clients are relying on that" },
            { loc: [0, 1], note: "Translate specific IDs to URIs" },
            { loc: [1, 2], note: "Make use of known property names" },
            { loc: [2, 3], note: "Build custom logic on those assumptions" },
            { loc: [3, 4], note: "Rely on JSON structure" },
          ]}/>

        <Slide>
          <Heading textColor="tertiary" size={6}>Of course, one friday, after a backend deployment</Heading>
          <Text textColor="error">Uncaught TypeError: Cannot read property 'street' of undefined</Text>
        </Slide>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/change.example")}
          ranges={[
            { loc: [0, 10], title: "Some minor adjustments" },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/changed_client.example")}
          ranges={[
            { loc: [0, 6], title: "Go team! It's a small fix." },
            { loc: [0, 1], note: "This still works, doesn't it?" },
            { loc: [1, 3], note: "Ok, CamelCase. So be it." },
            { loc: [4, 5], note: "Uh. Yeah, another request here. Need to hardcode '/addresses/{id}' endpoint." },
            { loc: [5, 6], note: "Works now. At least until street is renamed" },
          ]}/>

        <Slide>
          <Heading textColor="secondary" size={4}>This #!+%! does not need to break your code!</Heading>
          <Text textColor="tertiary">Property names, data  structure and even "endpoints" are details that should not really matter.</Text>
        </Slide>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/jsonld.example")}
          ranges={[
            { loc: [0, 11], title: "JSON-LD for the rescue" },
            { loc: [1, 2], note: "Add context, and therefore meaning, to your data" },
            { loc: [2, 3], note: "No magic numbers. Uniform Resource IDs (URIs)" },
            { loc: [3, 4], note: "Properties are URIs: http://schema.org/givenName" },
            { loc: [4, 5], note: "Resolve http://schema.org/familyName to get the meaning" },
            { loc: [5, 11], note: "Other resources can be embedded or linked via URI" },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/server.jsonld.example")}
          ranges={[
            { loc: [2, 11], title: "Server JSON can stay as it is!" },
            { loc: [1, 2], title: "Just provide a custom context" },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/server.context.example")}
          ranges={[
            { loc: [0, 11], title: "Server JSON-LD context" },
            { loc: [2, 3], note: "Based on schema.org vocabulary" },
            { loc: [3, 8], note: "This is how the server calls it" },
            { loc: [8, 9], note: "Using 'id' instead of fancy '@id'" },
            { loc: [9, 10], note: "IDs expand based on this URI" },
          ]}/>

          <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/client-without-context.example")}
          ranges={[
            { loc: [0, 10], title: "Without a context", note: "It's not feasible that way, but it gives some interesting possibilities " },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/client.context.example")}
          ranges={[
            { loc: [0, 10], title: "The client can apply it's own context" },
            { loc: [2, 7], note: "Decide how the properties are called here" },
            { loc: [7, 9], note: "Let's call ID 'uri' and use it relative to that base" },
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/client.compacted.example")}
          ranges={[
            { loc: [0, 10], title: "And that's what we get", note: "The server does not have a say in this anymore!"},
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/client.compaction.example")}
          ranges={[
            { loc: [0, 8], title: "How it's done"},
            { loc: [0, 1], note: "> npm install jsonld"},
            { loc: [2, 3], note: "This line is all the magic"},
            { loc: [4, 6], note: "The client is in full control here"},
            { loc: [7, 8], note: "This might still become an issue"},
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/changed.jsonld.example")}
          ranges={[
            { loc: [0, 8], title: "Server side changes"},
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/changed.context.example")}
          ranges={[
            { loc: [4, 6], title: "Server must adjust context"},
            { loc: [6, 9], note: "The address is now referred by ID"},
            { loc: [9, 12], note: "And has a different base URI"},
          ]}/>

        <CodeSlide
          bgColor="primary"
          transition={[]}
          lang="javascript"
          code={require("!raw-loader!./assets/changed-address.client.example")}
          ranges={[
            { loc: [0, 9], title: "No changes from client perspective", note: "This is what the person data looks like after applying the client context"},
            { loc: [2, 5], note: "The address is still an object, it is NOT undefined"},
            { loc: [9, 12], note: "This can stay the same"},
            { loc: [12, 13], note: "Won't break, but street will be undefined"},
            { loc: [13, 15], note: "BUT: we can fetch resources in a generic way to get all data eventually"},
            { loc: [9, 15], note: "This code will work independent of server side naming and structure"},
          ]}/>

        <Slide>
          <Text style={{margin: "1rem"}} textColor="tertiary">Data can be normalized to <em>triples</em>.</Text>
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderItem textSize={tableSize}>Subject</TableHeaderItem>
                <TableHeaderItem textSize={tableSize}>Predicate</TableHeaderItem>
                <TableHeaderItem textSize={tableSize}>Object</TableHeaderItem>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableItem textSize={tableSize}>https://app.example/persons/42</TableItem>
                <TableItem textSize={tableSize}>https://schema.org/givenName</TableItem>
                <TableItem textSize={tableSize}>John</TableItem>
              </TableRow>
              <TableRow>
                <TableItem textSize={tableSize}>https://app.example/persons/42</TableItem>
                <TableItem textSize={tableSize}>https://schema.org/familyName</TableItem>
                <TableItem textSize={tableSize}>Doe</TableItem>
              </TableRow>
              <TableRow>
                <TableItem textSize={tableSize}>https://app.example/persons/42</TableItem>
                <TableItem textSize={tableSize}>https://schema.org/address</TableItem>
                <TableItem textSize={tableSize}>https://app.example/addresses/1337</TableItem>
              </TableRow>
              <TableRow>
                <TableItem textSize={tableSize}>https://app.example/addresses/1337</TableItem>
                <TableItem textSize={tableSize}>https://schema.org/streetAddress</TableItem>
                <TableItem textSize={tableSize}>Mainstreet 42</TableItem>
              </TableRow>
            </TableBody>
          </Table>
          <Appear>
            <div >
            <Text style={{margin: "1rem"}} textColor="tertiary">It's all <em>resources</em> and <em>links</em> between them.</Text>
            <Image width="90%" fit src="./linked_data.svg"/>
            </div>
          </Appear>
        </Slide>

        <Slide>
          <Image width="90%" fit src="./linked_data.svg"/>
          <Text style={{margin: "2rem"}} textColor="tertiary">It's up to you, how to serialize this graph.</Text>
        </Slide>

        <Slide>
          <Heading textColor="tertiary" size={3}>(German) Blogposts</Heading>
          <List>
            <ListItem>Schmerzloser Datenaustausch: Mit JSON-LD Backend und Frontend entkoppeln <URL small href="https://blog.codecentric.de/2018/07/datenaustausch-mit-json-ld/"/></ListItem>

            <ListItem>JSON-LD: Verlinkte Daten statt hart-kodierter Endpunkte <URL small href="https://blog.codecentric.de/2018/07/json-ld-verlinkte-daten-statt-hart-kodierter-endpunkte/"/></ListItem>

            <ListItem>Linked Data mit JSON-LD: Semantische Fakten statt fester Baumstruktur <URL small href="https://blog.codecentric.de/2018/07/json-linked-data/"/></ListItem>
          </List>
        </Slide>

        <Slide>
          <Heading textColor="tertiary" size={3}>Links</Heading>
          <List>
            <ListItem>json-ld.org Homepage<URL small href="https://json-ld.org"/></ListItem>
            <ListItem>jsonld.js Library<URL small href="https://github.com/digitalbazaar/jsonld.js/"/></ListItem>
            <ListItem>JSON-LD Playground<URL small href="https://json-ld.org/playground/"/></ListItem>
          </List>
        </Slide>

        <Slide>
          <Heading caps fit>Questions?</Heading>
          <Heading textColor="quaternary" caps fit>Discussion!</Heading>
        </Slide>

      </Deck>
    );
  }
}
